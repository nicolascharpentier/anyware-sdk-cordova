Anyware Cordova plugin
======================

Installation
------------

### Cordova project

    cordova plugin add --save git@bitbucket.org:novom/anyware-sdk-cordova.git

### Meteor project

    meteor add cordova:com.novomnetworks.anyware.cordova.plugin@https://bitbucket.org/novom/anyware-sdk-cordova/get/<COMMIT_SHA>.tar.gz

#### Update

    meteor remove cordova:com.novomnetworks.anyware.cordova.plugin
    meteor add cordova:com.novomnetworks.anyware.cordova.plugin@https://bitbucket.org/novom/anyware-sdk-cordova/get/<COMMIT_SHA>.tar.gz

Usage
-----

### Start Engine

    anyware.start(YOUR_API_KEY);

### Stop Engine

    anyware.stop();

### Get Nearest Zone

    anyware.getNearestZone(function(err, zone){});

### Bind Custom Id

    anyware.bindCustomId(key, value);

### Get Device Id

    anyware.getDeviceId(function(err, deviceId){});

### Get Notifications

    anyware.getNotificationsSince(date, function(err, notifications){});    //date format: yyyy-MM-dd'T'HH:mm:ss'.'SSSZ

### Find Devices From Users Ids

    anyware.findDevicesFromUserIds(key, userIds, function(error, userLocations){});

### Listen For Events

    anyware.on(event_name, function(data){});

The listeners must be set **BEFORE** calling `anyware.start`.
The following events are generated by the plugin:

#### Engine events

- `engine_start`: The engine has started.
    - `data` = {}

- `engine_start_fail`: The engine has failed to start.
    - `data` = { "error": string }

- `engine_fail`: The engine has encountered an error (iOS only).
    - `data` = { "error": string }

#### Zone events

- `zone_enter`: The device just entered a zone.
    - `data` = { "name": string, "metadata": object }

- `zone_exit`: The device just exited a zone.
    - `data` = { "name": string, "metadata": object }

- `zone_stay`: The device stayed in a zone.
    - `data` = { "name": string, "metadata": object, "proximity": string }

#### Notification events

- `consume_custom_action_notification`: A custom action notification was received or clicked.
    - `data` = { "message": string, "date": string, "parameters": object }    //date format: yyyy-MM-dd'T'HH:mm:ss'.'SSSZ

- `consume_plain_text_notification`: A plain text notification was received or clicked.
    - `data` = { "message": string, "date": string }    //date format: yyyy-MM-dd'T'HH:mm:ss'.'SSSZ

- `consume_web_page_notification`: A web page notification was received or clicked.
    - `data` = { "message": string, "date": string, "url": string }    //date format: yyyy-MM-dd'T'HH:mm:ss'.'SSSZ

- `consume_notification_fail`: A error occurred while parsing an Anyware notification.
    - `data` = { "error": string }

Updating the SDK
----------------

### Android

1. Build the new version, and copy the .aar under src/android/libs/.
2. Extract the .aar, to update the contents of src/android/libs/anywaresdk
3. Check that plugin.xml and src/android/libs/anywaresdk/AndroidManifest.xml are in sync
4. That's it!

### iOS

1. Build new version, and copy the generated directory to src/ios/AnywareSDK/
2. Update the version number in plugin.xml to reflect the new one for src/ios/AnywareSDK/libAnywareSDK-x.y.z.a
3. That's it!