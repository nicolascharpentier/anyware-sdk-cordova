//
//  AWNotificationDelegate.h
//  Pods
//
//  Created by Emile Cantin on 2015-04-23.
//
//

#import <Foundation/Foundation.h>

@class AWREngine;

/*!
 The delegate you need to implement in order to receive Anyware Notification callbacks
 */
@protocol AWNotificationDelegate <NSObject>

- (void) anywareEngine:(AWREngine *)engine didFailToConsumeNotification:(UILocalNotification *)notification withError:(NSError *)error;

- (void) anywareEngine:(AWREngine *)engine didConsumePlainTextNotification:(UILocalNotification *)notification withMessage:(NSString *)message;

- (void) anywareEngine:(AWREngine *)engine didConsumeWebPageNotification:(UILocalNotification *)notification withMessage:(NSString *)message andURL:(NSString *)url;

- (void) anywareEngine:(AWREngine *)engine didConsumeCustomActionNotification:(UILocalNotification *)notification withMessage:(NSString *)message andParameters:(NSDictionary *)parameters;

@end
