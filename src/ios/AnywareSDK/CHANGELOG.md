# CHANGELOG

## 2.3.10

- Added the possibility of setting time conditions to proximity rules.

## 2.3.9

- Now retrieving config from the server in a background thread to not hang the app when config is large.

## 2.3.8

- Disabling notifications through `setNotificationsEnabled` now unregisters for remote notifications too.

## 2.3.7

- Now sending site entries and exits to server.
- Bug fix where the engine would crash when receiving push notifications.

## 2.3.6

- Removed engine's method `resumeEngine`.
- Bug fix where the engine would crash the app when the config has no root region.

## 2.3.5

- Removed all references to RestKit (and thus AFNetworking).
- Bug fix where the engine would fail to monitor GPS regions.
- Bug fix where visit durations would fail to be sent.

## 2.3.4

- Now waiting for user's authorization before trying to start monitoring user's location.
- Bug fix where a zone exit would never be triggered.
- Bug fix where the engine would never start or stop taking GPS locations.
- Bug fix where the integration of the SDK as a static library would fail due to a change in the database.

## 2.3.3

- Now calculating visit durations client-side for easier post-event analytics.
- Minor bug fixes.

## 2.3

- Modified the engine's `bindEngineWithUserId` method to add a `forKey` parameter. This way you can store multiple ids for a single user.
- Added the device finder feature.
- Added security to every request (now using https).
- Bug fix that could trigger a beacon zone exit upon receiving a GPS location.
- Bug fix that sometimes showed the wrong message in a generated notification.

## 2.2

- Added proximity rules management that creates different types of local notifications depending on specific conditions when entering or exiting a zone. Several new methods have been added to the engine about notifications.
- Added support for Push Notifications.
- Added a method that parses the launch options of the AppDelegate to determine what action should be taken by the engine. Its usage is documented in the README.md file.
- Added monitoring of precise GPS zones.
- Removed GPS usage choice (GPS locations are now updated only when the user is around GPS zones).
- Bug fixes related to support or more than 20 regions.
- Bug fix that could increase the frequency of data push to server.

## 2.1

- Added support for more than 20 regions.
- Added the getNearestZone method that is useful to get the approximated location of the device on demand.
- Added the resumeEngine and isRunning methods that are useful to decide when the engine should be running and when it should not.
- Bug fixes related to the data collection dates.

## 2.0.2

- Added the getDeviceId method for AWREngine.

## 1.1.3

- Bug fix for iPhone devices where the zone enter would not be triggered when the engine starts in the zone.

## 1.1.2

- Added power saving mode.
- Bug fix for proximity values on zone events.
- Bug fix for GPS location monitoring.
- Bug fix for iBeacon configuration management.

## 1.1.1

- Bug fixes.

## 1.1

- Added support for iOS 8.
- Added proximity with the didEnterZone callback.
- Added GPS usage choice.
- Added device information collection.
- Bug fixes.

## 1.0.4

- Added data usage restrictions when connected via 3G.

## 1.0.3

- Added persistence mode choice.
- Added allowed data collection dates.

## 1.0.2

- Added a method to the engine that shows a Blutooth activation popup if the Bluetooth is disabled.

## 1.0.1

- Initial release.
