//
//  AWREngineDelegate.h
//  AnywareSDK
//
//  Created by Novom Networks.
//  Copyright (c) 2014 Novom Networks Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class AWREngine;

/*!
The delegate you need to implement in order to receive Anyware Location Events
 */
@protocol AWREngineDelegate <NSObject>

/*!
 Called once the engine is started successfully
 */
- (void) didStartAnywareEngine:(AWREngine*)engine;

/*!
 Called if the Anyware Engine did fail to start
 */
- (void) anywareEngine:(AWREngine*)engine didFailToStartWithError:(NSError *)error;

/*!
 Called if the Anyware Engine did fail at some point after starting.
 */
- (void) anywareEngine:(AWREngine*)engine didFailWithError:(NSError *)error;

/*!
 Called when the state of a zone changes to inside
 */
- (void) anywareEngine:(AWREngine*)engine didEnterZone:(NSString*)name metadatas:(NSDictionary *)metaData proximity:(NSString*)proximity;

/*!
 Called when the state of a zone changes to outside
 */
- (void) anywareEngine:(AWREngine*)engine didExitZone:(NSString*)name metadatas:(NSDictionary *)metaData;

/*!
 Called when an additional detection is made while inside a zone
 */
- (void) anywareEngine:(AWREngine *)engine didStayInZone:(NSString*)name metadatas:(NSDictionary *)metaData proximity:(NSString*)proximity;

@end
