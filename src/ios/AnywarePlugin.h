#import <Cordova/CDVPlugin.h>
#import "AWREngine.h"

@interface AnywarePlugin : CDVPlugin<AWREngineDelegate, AWNotificationDelegate>

@property(nonatomic, strong) AWREngine *engine;

- (void)startEngine:(CDVInvokedUrlCommand *)command;
- (void)stopEngine:(CDVInvokedUrlCommand *)command;
- (void)getNearestZone:(CDVInvokedUrlCommand *)command;
- (void)bindEngineWithUserId:(CDVInvokedUrlCommand *)command;
- (void)getDeviceId:(CDVInvokedUrlCommand *)command;
- (void)getNotificationsSince:(CDVInvokedUrlCommand *)command;
- (void)findDevicesFromUserIds:(CDVInvokedUrlCommand *)command;
- (void)setNotificationsEnabled:(CDVInvokedUrlCommand *)command;
- (void)setupNotifications:(CDVInvokedUrlCommand *)command;

@end
