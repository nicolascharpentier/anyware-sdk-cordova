#import "AnywarePlugin.h"

@implementation AnywarePlugin

NSDictionary* launchOptions;

#pragma mark Class Life Cycle

/**
    The load method is called when the class is loaded. This mean that this block is executed before the app starts.
*/
+ (void) load {
    // when the app finishes launching (used to retrieve the launchOptions)
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didFinishLaunchingWithOptions:)
                                                 name:UIApplicationDidFinishLaunchingNotification
                                               object:nil];
}

/**
    This is called by cordova when it is ready to init the plugins. Basically, it is called few seconds after the app starts.
*/
- (CDVPlugin*) initWithWebView:(UIWebView*)theWebView {
    NSLog(@"Initializing Anyware plugin");
    self = (AnywarePlugin *)[super initWithWebView:(UIWebView*)theWebView];
    if (self) {
        [self addLocalAndRemoteNotificationObservers];
        self.engine = [[AWREngine alloc] initWithEngineDelegate:self andNotificationDelegate:self];
        [self.engine setLogLevel:LOG_LEVEL_DEBUG];
        [self.engine setMaxDataUsage:100];
        [self.engine registerForNotifications:[UIApplication sharedApplication]];
    }
    return self;
}

/**
    We add observers to be able to receive local and remote notifications when they are clicked by the user.
*/
- (void) addLocalAndRemoteNotificationObservers {
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];

    //when the app receives a local notification (the CDVLocalNotification notification is declared by the CDVPlugin class)
    [center addObserver:self
               selector:@selector(didReceiveLocalNotification:)
                   name:CDVLocalNotification
                 object:nil];

    //when the app is successfuly registered to the APS with a device token
    [center addObserver:self
               selector:@selector(didRegisterForRemoteNotificationsWithDeviceToken:)
                   name:@"AWDeviceToken"
                 object:nil];

    //when the app receives a remote notification
    [center addObserver:self
               selector:@selector(didReceiveRemoteNotification:)
                   name:@"AWRemoteNotification"
                 object:nil];
}

#pragma mark Observer delegate methods

/**
    Called when the app starts, thanks to the notification center used the in 'load' method. It saves the launchOptions to know what to do when 'startEngine' is called.
 */
+ (void) didFinishLaunchingWithOptions:(NSNotification*)notification {
    launchOptions = notification.userInfo;
}

- (void) didReceiveLocalNotification:(NSNotification*)notification {
    NSLog(@"didReceiveLocalNotification: %@", notification);
    [self.engine parseLocalNotification:notification.object];
}

- (void) didRegisterForRemoteNotificationsWithDeviceToken:(NSNotification*)notification {
    [self.engine bindEngineWithDeviceToken:notification.object];
}

- (void) didReceiveRemoteNotification:(NSNotification*)notification {
    NSLog(@"didReceiveRemoteNotification: %@", notification);
    [self.engine parseRemoteNotification:notification.object];
}

#pragma mark Private Methods

/**
    Called when the engine generates an event. It calls javascript code.
*/
- (void) fireEvent:(NSString *)event withData:(NSDictionary *)data {
    NSString* jsonData = [self convertDictionaryToJSON:data];
    NSString* js = [NSString stringWithFormat: @"anyware.notify('%@', %@);", event, jsonData];
    NSLog(@"Will call javascript: %@", js);
    [self.commandDelegate evalJs:js];
}

/**
    Because a simple stringWithFormat with an NSDictionary does not output valid JSON, we need to convert it to avoid javascript errors.
*/
- (NSString*) convertDictionaryToJSON:(NSDictionary *)dictionary {
    NSError *error; 
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary 
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];

    if (!jsonData) {
        NSLog(@"Error while parsing dictionary: %@", error);
        return @"";
    }
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (NSString*) stringRepresentationOfDate:(NSDate*)date {
    return [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterMediumStyle];
}

- (NSDate*) dateFromString:(NSString*)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'.'SSSZ"];
    return [dateFormatter dateFromString:dateString];
}

#pragma mark Public Methods

- (void) startEngine:(CDVInvokedUrlCommand *)command {
    NSString *uuid = [command argumentAtIndex:0];
    
    NSLog(@"Starting engine with API KEY: %@", uuid);
    NSLog(@"launchOptions: %@", launchOptions);
    if([self.engine shouldStartEngineBasedOnLaunchOptions:launchOptions]) {
        [self.engine startEngine:uuid withPersistentMode:YES];
    }

    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) stopEngine:(CDVInvokedUrlCommand *)command {
    NSLog(@"Stopping engine");
    [self.engine stopEngine:YES];

    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) getNearestZone:(CDVInvokedUrlCommand *)command {
    NSLog(@"Getting nearest zone");
    NSDictionary *zone = [self.engine getNearestZone];

    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:zone];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) bindEngineWithUserId:(CDVInvokedUrlCommand *)command {
    NSString *key = [command argumentAtIndex:0];
    NSString *userId = [command argumentAtIndex:1];
    
    NSLog(@"Binding engine with user id: %@ for key: %@", userId, key);
    [self.engine bindEngineWithUserId:userId forKey:key];

    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) getDeviceId:(CDVInvokedUrlCommand *)command {
    NSLog(@"Getting device id");
    NSNumber *deviceId = [self.engine getDeviceId];

    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:[deviceId intValue]];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) getNotificationsSince:(CDVInvokedUrlCommand *)command {
    NSDate *date = [self dateFromString:[command argumentAtIndex:0]];

    NSLog(@"Getting notifications since: %@", date);
    NSArray *notifications = [self.engine getNotificationsSince:date];
    
    NSMutableArray *jsonNotifications = [NSMutableArray new];
    NSLog(@"Will convert %@ notifications in json format", @(notifications.count));
    [notifications enumerateObjectsUsingBlock:^(NSDictionary *notification, NSUInteger idx, BOOL *stop) {
        NSLog(@"Converting notification: %@", notification);
        NSMutableDictionary *jsonNotification = [NSMutableDictionary new];
        [notification enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSLog(@"Adding key: %@", key);
            NSLog(@"With value: %@", obj);
            if([obj isKindOfClass:[NSDate class]]) {
                [jsonNotification setObject:[self stringRepresentationOfDate:obj] forKey:key];
            } else {
                [jsonNotification setObject:obj forKey:key];
            }
        }];
        NSLog(@"Converted notification: %@", jsonNotification);
        [jsonNotifications addObject:jsonNotification];
    }];

    NSLog(@"Converted notifications: %@", jsonNotifications);
    
    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:jsonNotifications];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) findDevicesFromUserIds:(CDVInvokedUrlCommand *)command {
    NSString *key = [command argumentAtIndex:0];
    NSArray *userIds = [command argumentAtIndex:1];

    NSLog(@"Finding devices from user ids: %@ with key: %@", userIds, key);
    NSArray *userLocations = [self.engine findDevicesFromUserIds:userIds withKey:key];

    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:userLocations];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) setNotificationsEnabled:(CDVInvokedUrlCommand *)command {
    BOOL enabled = [[command argumentAtIndex:0] boolValue];
    
    NSLog(@"%@ notifications", enabled ? @"Enabling" : @"Disabling");
    [self.engine setNotificationsEnabled:enabled];
    
    //return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void)setupNotifications:(CDVInvokedUrlCommand *)command {
    //do nothing and return a success
    CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

#pragma mark Anyware Engine Delegate

- (void) didStartAnywareEngine:(AWREngine*)engine {
    [self fireEvent:@"engine_start" withData:@{}];
}

- (void) anywareEngine:(AWREngine*)engine didFailToStartWithError:(NSError *)error {
    [self fireEvent:@"engine_start_fail" withData:@{@"error": error.domain}];
}

- (void) anywareEngine:(AWREngine*)engine didFailWithError:(NSError *)error {
    [self fireEvent:@"engine_fail" withData:@{@"error": error.domain}];
}

- (void) anywareEngine:(AWREngine*)engine didEnterZone:(NSString*)name metadatas:(NSDictionary *)metaData proximity:(NSString*)proximity {
    [self fireEvent:@"zone_enter" withData:@{@"name": name, @"metadata": metaData}];
}

- (void) anywareEngine:(AWREngine*)engine didExitZone:(NSString*)name metadatas:(NSDictionary *)metaData {
    [self fireEvent:@"zone_exit" withData:@{@"name": name, @"metadata": metaData}];
}

- (void) anywareEngine:(AWREngine *)engine didStayInZone:(NSString*)name metadatas:(NSDictionary *)metaData proximity:(NSString*)proximity {
    [self fireEvent:@"zone_stay" withData:@{@"name": name, @"metadata": metaData, @"proximity": proximity}];
}

#pragma mark Anyware Notification Delegate

- (void) anywareEngine:(AWREngine *)engine didConsumeCustomActionNotification:(UILocalNotification *)notification withMessage:(NSString *)message andParameters:(NSDictionary *)parameters {
    [self fireEvent:@"consume_custom_action_notification" withData:@{@"message": message, @"date": [self stringRepresentationOfDate:notification.fireDate], @"parameters": parameters}];
}

- (void) anywareEngine:(AWREngine *)engine didConsumePlainTextNotification:(UILocalNotification *)notification withMessage:(NSString *)message {
    [self fireEvent:@"consume_plain_text_notification" withData:@{@"message": message, @"date": [self stringRepresentationOfDate:notification.fireDate]}];
}

- (void) anywareEngine:(AWREngine *)engine didConsumeWebPageNotification:(UILocalNotification *)notification withMessage:(NSString *)message andURL:(NSString *)url {
    [self fireEvent:@"consume_web_page_notification" withData:@{@"message": message, @"date": [self stringRepresentationOfDate:notification.fireDate], @"url": url}];
}

- (void) anywareEngine:(AWREngine *)engine didFailToConsumeNotification:(UILocalNotification *)notification withError:(NSError *)error {
    [self fireEvent:@"consume_notification_fail" withData:@{@"error": error.domain}];
}

@end
